#! /usr/bin/env Rscript

cat(format(Sys.time(), "[%H:%M:%S]"), "Loading libraries\n")

# environment vars
suppressPackageStartupMessages(library(methods))
suppressPackageStartupMessages(library(Seurat))
suppressPackageStartupMessages(library(readr))
suppressPackageStartupMessages(library(tibble))
suppressPackageStartupMessages(library(Matrix))
suppressPackageStartupMessages(library(yaml))
suppressPackageStartupMessages(library(ggExtra))
suppressPackageStartupMessages(library(grid))
suppressPackageStartupMessages(library(gridExtra))
suppressPackageStartupMessages(library(ggplot2))

########################## FUNCTIONS ##########################
# Convenience function to make directory if it doesn't already exist
make_dir <- function(dir) {
  if (!dir.exists(dir)) {
    dir.create(dir)
  }
}

# Convenience function to timestamp output
cat_time <- function(x) {
  cat(format(Sys.time(), "[%H:%M:%S]"), x)
}
catf <- function(format, ...) {
  cat_time(sprintf(format, ...))
}

########################## ADS ##########################
cat_time("Annotating Ads\n")
ads <- readRDS("./seurat/mouse/ads/var_genes_1500/int_method_RPCA/PCs_40/res_0.3/seurat.rds")
ad1 <- WhichCells(ads, idents = c(6, 7, 8, 9, 10, 11))
ad2 <- WhichCells(ads, idents = 5)
ad3 <- WhichCells(ads, idents = 4)
ad4 <- WhichCells(ads, idents = 1)
ad5 <- WhichCells(ads, idents = 2)
ad6 <- WhichCells(ads, idents = 3)
ads$cell_type <- "temp"
ads$cell_type[ad1] <- "mAd1"
ads$cell_type[ad2] <- "mAd2"
ads$cell_type[ad3] <- "mAd3"
ads$cell_type[ad4] <- "mAd4"
ads$cell_type[ad5] <- "mAd5"
ads$cell_type[ad6] <- "mAd6"

temp <- WhichCells(ads, expression = depot %in% c("EPI", "POV"))
ads$depot[temp] <- "PG"
temp <- WhichCells(ads, expression = diet == "NCD")
ads$diet[temp] <- "Chow"
ing <- WhichCells(ads, expression = sample == "Mm_ING_18")
epi <- WhichCells(ads, expression = sample == "Mm_POV_03")
ads$name[ing] <- "Mm_POV_03-1"
ads$sample[ing] <- "Mm_POV_03"
ads$depot[ing] <- "PG"
ads$name[epi] <- "Mm_ING_18-1"
ads$sample[epi] <- "Mm_ING_18"
ads$depot[epi] <- "ING"
Idents(ads) <- "cell_type"
DefaultAssay(ads) <- "RNA"
ads <- NormalizeData(ads)
saveRDS(ads, "mouse_ads.rds")

########################## PREADS ##########################
cat_time("Annotating Preads\n")
preads <- readRDS("./seurat/mouse/preads/var_genes_1500/int_method_RPCA/PCs_20/res_0.3/seurat.rds")
pre1 <- WhichCells(preads, idents = c(7, 8))
pre2 <- WhichCells(preads, idents = c(1, 2, 3, 4))
pre3 <- WhichCells(preads, idents = 6)
pre4 <- WhichCells(preads, idents = 9)
pre5 <- WhichCells(preads, idents = c(10, 11))
pre6 <- WhichCells(preads, idents = 5)
preads$cell_type <- "temp"
preads$cell_type[pre1] <- "mASPC1"
preads$cell_type[pre2] <- "mASPC2"
preads$cell_type[pre3] <- "mASPC3"
preads$cell_type[pre4] <- "mASPC4"
preads$cell_type[pre5] <- "mASPC5"
preads$cell_type[pre6] <- "mASPC6"

temp <- WhichCells(preads, expression = depot %in% c("EPI", "POV"))
preads$depot[temp] <- "PG"
temp <- WhichCells(preads, expression = diet == "NCD")
preads$diet[temp] <- "Chow"
ing <- WhichCells(preads, expression = sample == "Mm_ING_18")
epi <- WhichCells(preads, expression = sample == "Mm_POV_03")
preads$name[ing] <- "Mm_POV_03-1"
preads$sample[ing] <- "Mm_POV_03"
preads$depot[ing] <- "PG"
preads$name[epi] <- "Mm_ING_18-1"
preads$sample[epi] <- "Mm_ING_18"
preads$depot[epi] <- "ING"
Idents(preads) <- "cell_type"
DefaultAssay(preads) <- "RNA"
preads <- NormalizeData(preads)
saveRDS(preads, "mouse_preads.rds")

########################## MES ##########################
cat_time("Annotating Mes\n")
mes <- readRDS("./seurat/mouse/mes/var_genes_500/int_method_RPCA/PCs_10/res_0.1/seurat.rds")
#mes markers: all mes- BNC1, PTPRQ, mes1- more of a negative, mes2: PDZRN4, TRPM3, mes4: GYPA, HHIP (combined now into mes1), mes3: SGCZ, CNTN6, FOXP2
mes1 <- WhichCells(mes, idents = 2)
mes2 <- WhichCells(mes, idents = 1)
mes3 <- WhichCells(mes, idents = 3)
mes$cell_type <- "temp"
mes$cell_type[mes1] <- "mMes1"
mes$cell_type[mes2] <- "mMes2"
mes$cell_type[mes3] <- "mMes3"

DefaultAssay(mes) <- "RNA"
mes <- NormalizeData(mes)
temp <- WhichCells(mes, expression = depot %in% c("EPI", "POV"))
mes$depot[temp] <- "PG"
temp <- WhichCells(mes, expression = diet == "NCD")
mes$diet[temp] <- "Chow"
epi <- WhichCells(mes, expression = sample == "Mm_POV_03")
mes$name[epi] <- "Mm_POV_03-1"
Idents(mes) <- "cell_type"
saveRDS(mes, "mouse_mes.rds")

########################## VASC ##########################
cat_time("Annotating Vascular Cells\n")
vasc <- readRDS("./seurat/mouse/vasc/var_genes_1500/int_method_RPCA/PCs_30/res_0.8/seurat.rds")
#Marker genes: LEC1- RELN LEC2- FOXC2, CLDN11, both LEC- PROX1 venule- CA4, stalk- ACKR1, PLVAP, stalk1 - DOCK11, TPO, ELOVL7, 
#stalk2- BMPER, SAMD5, PKNOX2, art- SEMA3G, art1 - NKAIN2, HTR4, TOX, art2- NEBL, SIDT1, smc- ACTA2, MYH11, smc1- ITGA8, HPSE2, smc2- CLSTN2, NCKAP5, peri- RGS5, POSTN
ven <- WhichCells(vasc, idents = c(8, 9))
stalk1 <- WhichCells(vasc, idents = 10)
stalk2 <- WhichCells(vasc, idents = 11)
art1 <- WhichCells(vasc, idents = c(2, 3, 4))
art2 <- WhichCells(vasc, idents = 1)
peri <- WhichCells(vasc, idents = 13)
sm <- WhichCells(vasc, idents = 14)
lec1 <- WhichCells(vasc, idents = 5)
lec2 <- WhichCells(vasc, idents = 6)

vasc$cell_type <- "temp"
vasc$cell_type[sm] <- "mSMC"
vasc$cell_type[peri] <- "mPeri"
vasc$cell_type[lec1] <- "mLEC1"
vasc$cell_type[lec2] <- "mLEC2"
vasc$cell_type[ven] <- "mEndoV"
vasc$cell_type[stalk1] <- "mEndoS1"
vasc$cell_type[stalk2] <- "mEndoS2"
vasc$cell_type[art1] <- "mEndoA1"
vasc$cell_type[art2] <- "mEndoA2"

cells <- WhichCells(vasc, expression = cell_type == "temp", invert = TRUE)
vasc <- subset(vasc, cells = cells)
vasc <- RunUMAP(vasc, dims = 1:30, return.model = T)
temp <- WhichCells(vasc, expression = depot %in% c("EPI", "POV"))
vasc$depot[temp] <- "PG"
temp <- WhichCells(vasc, expression = diet == "NCD")
vasc$diet[temp] <- "Chow"
ing <- WhichCells(vasc, expression = sample == "Mm_ING_18")
epi <- WhichCells(vasc, expression = sample == "Mm_POV_03")
vasc$name[ing] <- "Mm_POV_03-1"
vasc$sample[ing] <- "Mm_POV_03"
vasc$depot[ing] <- "PG"
vasc$name[epi] <- "Mm_ING_18-1"
vasc$sample[epi] <- "Mm_ING_18"
vasc$depot[epi] <- "ING"
Idents(vasc) <- "cell_type"
DefaultAssay(vasc) <- "RNA"
vasc <- NormalizeData(vasc)
saveRDS(vasc, "mouse_vasc.rds")

########################## EPI ##########################
epi <- readRDS("/broad/rosenlab_archive/Projects/Margo-SingleCell/Analyses/mouse14/mouse_epithelial.rds")
temp <- WhichCells(epi, expression = depot %in% c("EPI", "POV"))
epi$depot[temp] <- "PG"
temp <- WhichCells(epi, expression = diet == "NCD")
epi$diet[temp] <- "Chow"
ing <- WhichCells(epi, expression = sample == "Mm_ING_18")
epi2 <- WhichCells(epi, expression = sample == "Mm_POV_03")
epi$name[ing] <- "Mm_POV_03-1"
epi$sample[ing] <- "Mm_POV_03"
epi$depot[ing] <- "PG"
epi$name[epi2] <- "Mm_ING_18-1"
epi$sample[epi2] <- "Mm_ING_18"
epi$depot[epi2] <- "ING"
DefaultAssay(epi) <- "RNA"
epi <- NormalizeData(epi)
Idents(epi) <- "cell_type"
epi_cells <- WhichCells(epi)
saveRDS(epi, "mouse_epithelial.rds")

########################## IMM ##########################
cat_time("Annotating Immune Cells\n")
immune <- readRDS("./seurat/mouse/imm/var_genes_1500/int_method_RPCA/PCs_40/res_0.3/seurat.rds")
#marker genes: mast - CPA3, bcell- MS4A1, DC4: FCGR3A, SERPINA1 DC3: LAMP3, DUSP4
mast <- WhichCells(immune, idents = 9)
DC2 <- WhichCells(immune, idents = 5)
DC3 <- WhichCells(immune, idents = 8)
mac4 <- WhichCells(immune, idents = 2)
bcell <- WhichCells(immune, idents = 4)
macs <- readRDS("./seurat/mouse/imm/macs/var_genes_1000/int_method_RPCA/PCs_20/res_0.4/seurat.rds")
#mac markers: mac1: F13A1 WWP1 SLC9A9 mac2: TREM2, LPL, mac3: MARCO, TIMD4, mac4: RELB, CCL3, mono: VCAN, S100A9
#DC markers: DC1: CD1C, FCER1A, DC2: CLEC9A, IRF8, IDO1
mac1 <- WhichCells(macs, idents = c(8, 12, 14, 15))
mac2 <- WhichCells(macs, idents = c(1, 4, 5, 6, 7, 9, 13))
mac3 <- WhichCells(macs, idents = 11)
mono <- WhichCells(macs, idents = c(2, 10))
DC1 <- WhichCells(macs, idents = 3)
tnk <- readRDS("./seurat/mouse/imm/tnk/var_genes_1500/int_method_RPCA/PCs_20/res_0.2/seurat.rds")
#tcell markers: all: IL7R t1 CD4, ERN1, ADAM19 t2: CD8A, CCL5, KLRG1, t3: SELL, FHIT, CCR7, NELL2, FOXP1, t4 (treg): FOXP3, CTLA4, IL2RA
#nk markers: GNLY, NKG7 nk1: KCNQ5, RAP1GAP2 nk2: ZEB2, ATP8B4, LDB2, HIP1
t1 <- WhichCells(tnk, idents = c(1, 3))
t2 <- WhichCells(tnk, idents = 4)
t3 <- WhichCells(tnk, idents = 5)
nk <- WhichCells(tnk, idents = 2)
mono3 <- readRDS("./seurat/mouse/imm/monos/var_genes_1500/int_method_RPCA/PCs_30/res_0.2/seurat.rds")
mono2 <- WhichCells(mono3, idents = c(2, 3))
neu <- WhichCells(mono3, idents = 1)

immune$cell_type <- "temp"
immune$cell_type[mast] <- "mMast"
immune$cell_type[bcell] <- "mBcell"
immune$cell_type[mac1] <- "mMac1"
immune$cell_type[mac2] <- "mMac2"
immune$cell_type[mac3] <- "mMac3"
immune$cell_type[mac4] <- "mMac4"
immune$cell_type[mono] <- "mMono1"
immune$cell_type[DC1] <- "mcDC2"
immune$cell_type[DC2] <- "mcDC1"
immune$cell_type[DC3] <- "mDC3"
immune$cell_type[mono2] <- "mMono2"
immune$cell_type[t1] <- "mTcell1"
immune$cell_type[t2] <- "mTcell2"
immune$cell_type[t3] <- "mTcell3"
immune$cell_type[nk] <- "mNK"
immune$cell_type[neu] <- "mNeu"

cells <- WhichCells(immune, expression = cell_type == "temp", invert = TRUE)
immune <- subset(immune, cells = cells)
immune <- RunUMAP(immune, dims = 1:40, return.model = T)
temp <- WhichCells(immune, expression = depot %in% c("EPI", "POV"))
immune$depot[temp] <- "PG"
temp <- WhichCells(immune, expression = diet == "NCD")
immune$diet[temp] <- "Chow"
ing <- WhichCells(immune, expression = sample == "Mm_ING_18")
epi <- WhichCells(immune, expression = sample == "Mm_POV_03")
immune$name[ing] <- "Mm_POV_03-1"
immune$sample[ing] <- "Mm_POV_03"
immune$depot[ing] <- "PG"
immune$name[epi] <- "Mm_ING_18-1"
immune$sample[epi] <- "Mm_ING_18"
immune$depot[epi] <- "ING"
Idents(immune) <- "cell_type"
DefaultAssay(immune) <- "RNA"
immune <- NormalizeData(immune)
saveRDS(immune, "mouse_immune.rds")
# myeloid <- subset(immune, idents = c("mMac1", "mMac2", "mMac3", "mMac4", "mMono1", "mMono2", "mDC1", "mDC2", "mDC3", "mMast", "mNeu"))
# DefaultAssay(myeloid) <- "integrated"
# myeloid <- RunPCA(myeloid)
# myeloid <- RunUMAP(myeloid, dims = 1:30, return.model = T)
# DefaultAssay(myeloid) <- "RNA"
# myeloid <- NormalizeData(myeloid)
# saveRDS(myeloid, "mouse_myeloid.rds")
# lymphoid <- subset(immune, idents = c("mTcell1", "mTcell2", "mTcell3", "mBcell1", "mBcell2", "mNK"))
# DefaultAssay(lymphoid) <- "integrated"
# lymphoid <- RunPCA(lymphoid)
# lymphoid <- RunUMAP(lymphoid, dims = 1:30, return.model = T)
# DefaultAssay(lymphoid) <- "RNA"
# lymphoid <- NormalizeData(lymphoid)
# saveRDS(lymphoid, "mouse_lymphoid.rds")

########################## ALL-CELL ##########################
cat_time("Annotating Seurat Object\n")
seurat <- readRDS("/broad/rosenlab_archive/Projects/Margo-SingleCell/Analyses/mouse14/nCells_2/nUMIs_400/mito_0.1/batch_default/var_genes_3000/int_method_RPCA/PCs_50/res_0.3/seurat.rds")

seurat$cell_type <- "temp"
seurat$cell_type[mast] <- "mMast"
seurat$cell_type[bcell] <- "mBcell"
seurat$cell_type[mac1] <- "mMac1"
seurat$cell_type[mac2] <- "mMac2"
seurat$cell_type[mac3] <- "mMac3"
seurat$cell_type[mac4] <- "mMac4"
seurat$cell_type[mono] <- "mMono1"
seurat$cell_type[DC1] <- "mcDC2"
seurat$cell_type[DC2] <- "mcDC1"
seurat$cell_type[DC3] <- "mDC3"
seurat$cell_type[mono2] <- "mMono2"
seurat$cell_type[t1] <- "mTcell1"
seurat$cell_type[t2] <- "mTcell2"
seurat$cell_type[t3] <- "mTcell3"
seurat$cell_type[nk] <- "mNK"
seurat$cell_type[neu] <- "mNeu"
seurat$cell_type[sm] <- "mSMC"
seurat$cell_type[peri] <- "mPeri"
seurat$cell_type[lec1] <- "mLEC1"
seurat$cell_type[lec2] <- "mLEC2"
seurat$cell_type[ven] <- "mEndoV"
seurat$cell_type[stalk1] <- "mEndoS1"
seurat$cell_type[stalk2] <- "mEndoS2"
seurat$cell_type[art1] <- "mEndoA1"
seurat$cell_type[art2] <- "mEndoA2"
seurat$cell_type[mes1] <- "mMes1"
seurat$cell_type[mes2] <- "mMes2"
seurat$cell_type[mes3] <- "mMes3"
seurat$cell_type[pre1] <- "mASPC1"
seurat$cell_type[pre2] <- "mASPC2"
seurat$cell_type[pre3] <- "mASPC3"
seurat$cell_type[pre4] <- "mASPC4"
seurat$cell_type[pre5] <- "mASPC5"
seurat$cell_type[pre6] <- "mASPC6"
seurat$cell_type[ad1] <- "mAd1"
seurat$cell_type[ad2] <- "mAd2"
seurat$cell_type[ad3] <- "mAd3"
seurat$cell_type[ad4] <- "mAd4"
seurat$cell_type[ad5] <- "mAd5"
seurat$cell_type[ad6] <- "mAd6"
seurat$cell_type[epi_cells] <- epi$cell_type

seurat$cell_type2 <- "temp"
seurat$cell_type2[sm] <- "SMC"
seurat$cell_type2[peri] <- "pericyte"
seurat$cell_type2[lec1] <- "LEC"
seurat$cell_type2[lec2] <- "LEC"
seurat$cell_type2[ven] <- "endothelial"
seurat$cell_type2[stalk1] <- "endothelial"
seurat$cell_type2[stalk2] <- "endothelial"
seurat$cell_type2[art1] <- "endothelial"
seurat$cell_type2[art2] <- "endothelial"
seurat$cell_type2[art2] <- "endothelial"
seurat$cell_type2[mast] <- "mast_cell"
seurat$cell_type2[bcell] <- "b_cell"
seurat$cell_type2[mac1] <- "macrophage"
seurat$cell_type2[mac2] <- "macrophage"
seurat$cell_type2[mac3] <- "macrophage"
seurat$cell_type2[mac4] <- "macrophage"
seurat$cell_type2[mono] <- "monocyte"
seurat$cell_type2[DC1] <- "dendritic_cell"
seurat$cell_type2[DC2] <- "dendritic_cell"
seurat$cell_type2[DC3] <- "dendritic_cell"
seurat$cell_type2[mono2] <- "monocyte"
seurat$cell_type2[neu] <- "neutrophil"
seurat$cell_type2[t1] <- "t_cell"
seurat$cell_type2[t2] <- "t_cell"
seurat$cell_type2[t3] <- "t_cell"
seurat$cell_type2[nk] <- "nk_cell"
seurat$cell_type2[ad1] <- "adipocyte"
seurat$cell_type2[ad2] <- "adipocyte"
seurat$cell_type2[ad3] <- "adipocyte"
seurat$cell_type2[ad4] <- "adipocyte"
seurat$cell_type2[ad5] <- "adipocyte"
seurat$cell_type2[ad6] <- "adipocyte"
seurat$cell_type2[mes1] <- "mesothelium"
seurat$cell_type2[mes2] <- "mesothelium"
seurat$cell_type2[mes3] <- "mesothelium"
seurat$cell_type2[pre1] <- "ASPC"
seurat$cell_type2[pre2] <- "ASPC"
seurat$cell_type2[pre3] <- "ASPC"
seurat$cell_type2[pre4] <- "ASPC"
seurat$cell_type2[pre5] <- "ASPC"
seurat$cell_type2[pre6] <- "ASPC"
seurat$cell_type2[epi_cells] <- epi$cell_type2

seurat$ct3 <- seurat$cell_type2
seurat$ct3[ad1] <- "mAd1"
seurat$ct3[ad2] <- "mAd2"
seurat$ct3[ad3] <- "mAd3"
seurat$ct3[ad4] <- "mAd4"
seurat$ct3[ad5] <- "mAd5"
seurat$ct3[ad6] <- "mAd6"

cells <- WhichCells(seurat, expression = cell_type == "temp", invert = TRUE)
seurat <- subset(seurat, cells = cells)
DefaultAssay(seurat) <- "integrated"
seurat <- RunPCA(seurat)
seurat <- RunUMAP(seurat, dims = 1:50)
temp <- WhichCells(seurat, expression = depot %in% c("EPI", "POV"))
seurat$depot[temp] <- "PG"
temp <- WhichCells(seurat, expression = diet == "NCD")
seurat$diet[temp] <- "Chow"
ing <- WhichCells(seurat, expression = sample == "Mm_ING_18")
epi2 <- WhichCells(seurat, expression = sample == "Mm_POV_03")
seurat$name[ing] <- "Mm_POV_03-1"
seurat$sample[ing] <- "Mm_POV_03"
seurat$depot[ing] <- "PG"
seurat$name[epi2] <- "Mm_ING_18-1"
seurat$sample[epi2] <- "Mm_ING_18"
seurat$depot[epi2] <- "ING"
Idents(seurat) <- "cell_type2"
DefaultAssay(seurat) <- "RNA"
seurat <- NormalizeData(seurat)
catf("Number of Cells in Whole Object: %s\n",as.character(dim(seurat)[2]))
saveRDS(seurat, "mouse_all.rds")

# pdf("plots.pdf")
# 	print(UMAPPlot(seurat, group.by = "cell_type", label = TRUE) + NoLegend())
# 	print(UMAPPlot(seurat, group.by = "cell_type2", label = TRUE))
# 	print(UMAPPlot(immune, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(endo, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(mes, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(preads, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(ads, group.by = "cell_type", label = TRUE))
# 	print(UMAPPlot(epi, group.by = "cell_type", label = TRUE))
# invisible(dev.off())

meta <- cbind(seurat$cell_type2, seurat$cell_type)
colnames(meta) <- c("level1", "level2")
make_dir("./cellect")
write.csv(meta, "./cellect/mouse-meta.csv")
# data <- seurat@assays$RNA@counts
# write(colnames(data), file = "mcolnames.txt")
# write(rownames(data), file = "mrownames.txt")
# writeMM(data, file = "mouse-counts.txt")

########################## SUB-OBJECTS ##########################
# seurat <- readRDS("mouse_all.rds")
cells <- WhichCells(seurat, expression = depot == "ING")
temp <- subset(seurat, cells = cells)
DefaultAssay(temp) <- "integrated"
temp <- RunPCA(temp)
temp <- RunUMAP(temp, dims = 1:30)
DefaultAssay(temp) <- "RNA"
# sat1 <- UMAPPlot(temp, group.by = "cell_type", label = TRUE)
# sat2 <- UMAPPlot(temp, group.by = "cell_type2", label = TRUE)
catf("Number of ING cells: %s\n",as.character(dim(temp)[2]))
saveRDS(temp, "mouse_ing.rds")

cells <- WhichCells(seurat, expression = depot == "PG")
temp <- subset(seurat, cells = cells)
DefaultAssay(temp) <- "integrated"
temp <- RunPCA(temp)
temp <- RunUMAP(temp, dims = 1:30)
DefaultAssay(temp) <- "RNA"
# vat1 <- UMAPPlot(temp, group.by = "cell_type", label = TRUE)
# vat2 <- UMAPPlot(temp, group.by = "cell_type2", label = TRUE)
catf("Number of PG cells: %s\n",as.character(dim(temp)[2]))
saveRDS(temp, "mouse_pg.rds")

# pdf("subset-umaps.pdf")
# 	print(sat1 + ggtitle("ING all"))
# 	print(sat2 + ggtitle("ING all"))
# 	print(vat1 + ggtitle("PG all"))
# 	print(vat2 + ggtitle("PG all"))
# invisible(dev.off())
